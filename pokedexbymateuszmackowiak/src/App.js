
import './MainPage.css';
import Axios from 'axios'
import React,{useState,useEffect} from 'react';
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import { BrowserRouter as Router } from 'react-router-dom'
import Pokemon from './Pokemon.js'
import MainPage from "./MainPage";


function App()
{
    return(
        <Router>
            <div className="App">
            <Switch>
                <Route exact path="/" component={MainPage} />
                <Route path="/pokemon/:pokemonID" component={Pokemon} />
            </Switch>
            </div>
        </Router>
    )
}
export default App;