# PokeDex by Mateusz Maćkowiak

## Description

Project made for JIT solutions/JIT team, for internship, by Mateusz Maćkowiak.

*What can be done in it?*

There can be found more than 13000 different pokemon cards displayed on screen in web browser.
There is option to filter them by their name, types and subtypes.
Each card can be clicked on main page for detailed description.
By default 250 cards are displayed, as internship task demanded.
Ammount of displayed cards can be easilty changed (More at the bottom of README).
Project uses https://pokemontcg.io/ api on behalf of its MIT license.

Works also on mobile devices.

## Visuals

![Optional Text1](ImagesForREADME/MainPage.png)
![Optional Text2](ImagesForREADME/BulbasaurCard.png)
![Optional Text3](ImagesForREADME/BulbasaurCardInfo.png)

## Installation

start project by downloading, opening project(click folder that is named "pokedexbymateuszmackowiak" with right click, then select, for example, Visual Studio Code, or open it by console).

Then type in terminal: 

npm install 
npm start

It will add node_modules folder to project, and then start localhost page in your default browser

**How to display more than 250 cards?**

Below there is reference to MainPage.js file, that is responsible for, well, main page

line 25-31 responsible for get requests to pokemon api https://pokemontcg.io/

useEffect(() => {
    fetchpokemonCards(1);// THE NUMBER TYPED IN THIS FIELD, DETERMINES HOW MANY REQUESTS FOR PAGES WILL BE MADE, so for example '2' means there will be 2x250 cards on first page
    // Warning -> the more you request, the longer loading will take, also, 54 page is last page with any pokemons, pages after that will only generate useless request and cause
    // faster throttle ban for day
    //
    // By standard there will be just 1 page downloaded, as I was told to do.
    //
    // By 30.05.2021, there will be 13438 cards downloaded if you type in 54, if you type in 53, then 13250, there are 250 cards per page with exception of page 54
  }, [])

line 25-31

Pokemon.js file is responsible by displaying pokemon card from url
root/pokemon/{pokemonID}

for example, after starting project

http://localhost:3000/pokemon/det1-1





