
import './MainPage.css';
import Axios from 'axios'
import React,{useState,useEffect} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import Pokemon from './Pokemon.js'


export default function MainPage() {
  Axios.defaults.headers.common = {
    "X-API-Key": "5c679eca-0d2c-4116-bb1e-3413b7dc5130",
  };
    const [searchedPokemonName,updateSearchedPokemonName]=useState("")
    const [searchedPokemonType,updateSearchedPokemonType]=useState("")
    const [searchedPokemonSubtype,updateSearchedPokemonSubtype]=useState("")
    const [pokemonCards,actualizePokemonCards]=useState([])
  useEffect(() => {
    fetchpokemonCards(1);// THE NUMBER TYPED IN THIS FIELD, DETERMINES HOW MANY REQUESTS FOR PAGES WILL BE MADE, so for example '2' means there will be 2x250 cards on first page
    // Warning -> the more you request, the longer loading will take, also, 54 page is last page with any pokemons, pages after that will only generate useless request and cause
    // faster throttle ban for day
    //
    // By standard there will be just 1 page downloaded, as I was told to do.
    //
    // By 30.05.2021, there will be 13438 cards downloaded if you type in 54, if you type in 53, then 13250, there are 250 cards per page with exception of page 54
  }, [])
  useEffect(() => {
    console.log(pokemonCards)
  }, [pokemonCards])
  
  const fetchpokemonCards=async(pagenumber)=>{
    let responsesDatas=[]
    for (let i = 1; i <= pagenumber; i++) {
      responsesDatas.push((await Axios('https://api.pokemontcg.io/v2/cards?page='+i)).data);
    }
    let sumOfResponsesDatas={data:[]}
    for (let i = 0; i <= pagenumber-1; i++) {
      sumOfResponsesDatas.data=sumOfResponsesDatas.data.concat(responsesDatas[i].data)
    }
    
    // it should present ONLY pokemon list on first page with photo and its name
    actualizePokemonCards(sumOfResponsesDatas)    
  }


  
  
  return (
    <div class="mainDiv">
      <div class="navbar">
        <b class="navbarTitle">Search for your pokemon!</b>
        <div class="navbarFlexContainer">
        <b>Name: </b>
        <input placeholder="Write Name" class="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "Write Name"}
        type="text" value={searchedPokemonName} onChange={(event)=>updateSearchedPokemonName(event.target.value)} />
        <b>Type: </b>
        <input placeholder="Write Type" class="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "Write Type"}
        type="text" value={searchedPokemonType} onChange={(event)=>updateSearchedPokemonType(event.target.value)} />
        <b>Subtype: </b>
        <input placeholder="Write Subtype" class="input" onFocus={(e) => e.target.placeholder = ''} onBlur={(e) => e.target.placeholder = "Write Subtype"}
        type="text" value={searchedPokemonSubtype} onChange={(event)=>updateSearchedPokemonSubtype(event.target.value)} />
        </div>
      </div>
      <div class="pokemonFlexContainer">  
      {
        searchedPokemonName===""&&searchedPokemonType===""&&searchedPokemonSubtype===""&&

        pokemonCards.data && pokemonCards.data.map(pokemon=>{
          return(
            <Link to={`/pokemon/${pokemon.id}`} class="pokemonFlexItem"
            onClick={()=>{updateSearchedPokemonName("");updateSearchedPokemonSubtype("");updateSearchedPokemonType("")}}>
              <div key={pokemon.id} style={{alignItems:'center',margin:'20px 60px'}}>
            <h4>{pokemon.name}</h4>
            <img width="400px" height="300px"
            src={pokemon.images.small}
            alt={pokemon.id+"_image_alt"}
            />
          </div>
          </Link>
            
          )

        })
      }
      {
        (searchedPokemonName!==""||searchedPokemonType!==""||searchedPokemonSubtype!=="")&&

        pokemonCards.data && pokemonCards.data
        .filter
        (
          function(pokemon)
          {
            if(searchedPokemonType!=="")
            {
              if(pokemon.types!==undefined)
              {
                return pokemon.types.map(type => type.toLowerCase()).some(e => e.includes(searchedPokemonType.toLowerCase()))
              }
              else
              {
                return false
              }
            }
            else
            {
              return true
            }
          }
        )
        .filter
        (
          function(pokemon)
          {
            if(searchedPokemonSubtype!=="")
            {
              if(pokemon.subtypes!==undefined)
              {
                return pokemon.subtypes.map(subtype => subtype.toLowerCase()).some(e => e.includes(searchedPokemonSubtype.toLowerCase()))
              }
              else
              {
                return false
              }
            }
            else
            {
              return true
            }
          }
        )
        .filter
        (
          function(pokemon)
          {
            if(searchedPokemonName!=="")
            {
              return pokemon.name.toLowerCase().includes(searchedPokemonName.toLowerCase())
            }
            else
            {
              return true
            }
          }
        )
        .map(pokemon=>{
          return(
            <Link to={`/pokemon/${pokemon.id}`} class="pokemonFlexItem" 
            onClick={()=>{updateSearchedPokemonName("");updateSearchedPokemonSubtype("");updateSearchedPokemonType("")}}>
              <div key={pokemon.id} style={{alignItems:'center',margin:'20px 60px'}}>
            <h4>{pokemon.name}</h4>
            <img 
            src={pokemon.images.small}
            alt={pokemon.id+"_image_alt"}
            />
          </div>
          </Link>
            
          )

        })
      }
      </div>
      
    </div>
  );
  
}



/*

Jit Team internship frontend exercise - create your own Pokedex !
Main points (MUST HAVE):
DONE • use React, pure js or typescript.
DONE • use pokemon api https://pokemontcg.io/
DONE • use http requests (can be axios)
NOT DONE(I still need to get to know what deos npm test is supposed to do)• it should be possible to run your app by: npm install,npm start and test by npm test
DONE(by first page I think it means https://api.pokemontcg.io/v2/cards?page=1) • it should present ONLY pokemon list on first page with photo and its name
NOT DONE • it should show description after pokemon element click
NOT DONE • description should be accessible by url (.../pokemon/id)
DONE • user should be able to search items
DONE • it should search items by name, type and subtype
NOT DONE • a readable readme file
Additional points
NOT DONE • spinners if screens are loading
NOT DONE • saving items to favourites
DONE • filtering pokemons by types
NOT DONE • pagination
DONE • good looking ux/ui
DONE • responsiveness
NOT DONE • error handling
Upload your solution to a public repository and send the url to maciej.kankowski@jit.team

*/