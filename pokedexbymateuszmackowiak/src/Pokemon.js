import './MainPage.css';
import Axios from 'axios'
import React,{useState,useEffect} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";

import './Pokemon.css';
function Pokemon({match})
{
  Axios.defaults.headers.common = {
    "X-API-Key": "5c679eca-0d2c-4116-bb1e-3413b7dc5130",
  };
  const [pokemonCards,actualizePokemonCards]=useState([])
  useEffect(() => {
    fetchpokemonCards();
  }, [])
  useEffect(() => {
    console.log(pokemonCards)
  }, [pokemonCards])
  
  const fetchpokemonCards=async()=>{
    const response=await Axios('https://api.pokemontcg.io/v2/cards/'+match.params.pokemonID);// it should present ONLY pokemon list on first page with photo and its name
    actualizePokemonCards(response.data)    
  }
  return (
    <div class="flexContainer">
      <div class="imageDiv">
      <img class="image"
            src={pokemonCards.data && pokemonCards.data.images.large}
            alt={pokemonCards.data && pokemonCards.data.id+"_image_alt"}
            />
      </div>
      
      <div class="descriptionContainer">
        <div>
          <b class="descriptionElement">Pokemon Name: </b>
          <b class="descriptionValue">{pokemonCards.data && pokemonCards.data.name}</b>
        </div>
        <div>
          <b class="descriptionElement">HP: </b>
          <b class="descriptionValue">{pokemonCards.data && pokemonCards.data.hp!==undefined && pokemonCards.data.hp}</b>
        </div>
        <div>
          <b class="descriptionElement">Level: </b>
          <b class="descriptionValue">{pokemonCards.data && pokemonCards.data.level!==undefined && pokemonCards.data.level}</b>
        </div>
        <div>
          <b class="descriptionElement">Pokemon Types: </b>
           {pokemonCards.data && pokemonCards.data.types!==undefined && pokemonCards.data.types.map(pokemonType=><b class="descriptionValue">{pokemonType}, </b>)}
        </div>
        <div>
          <b class="descriptionElement">Pokemon Subtypes: </b>
          {pokemonCards.data && pokemonCards.data.subtypes!==undefined && pokemonCards.data.subtypes.map(pokemonSubtype=><b class="descriptionValue">{pokemonSubtype}, </b>)}
        </div>
        <div>
          <b class="descriptionElement">Pokemon Abilities: </b>
          {
          pokemonCards.data && pokemonCards.data.abilities!==undefined && pokemonCards.data.abilities.map(element=>
            {
              return <div>
                <div><b class="underDescriptionElement">------------------------------------</b></div>
                <div><b class="underDescriptionElement">Ability Name: </b><b class="descriptionValue">{element.name} </b></div>
                <div><b class="underDescriptionElement">Text: </b><b class="descriptionValue">{element.text} </b></div>
                <div><b class="underDescriptionElement">Type: </b><b class="descriptionValue">{element.type}</b></div>
                <div><b class="underDescriptionElement">------------------------------------</b></div>
                </div>
            })}
        </div>
        <div>
          <b class="descriptionElement">Pokemon Attacks: </b>
          {
          pokemonCards.data && pokemonCards.data.attacks!==undefined && pokemonCards.data.attacks.map(element=>
            {
              return <div>
                <div><b class="underDescriptionElement">------------------------------------</b></div>
                <div><b class="underDescriptionElement">Attack Name: </b><b class="descriptionValue">{element.name} </b></div>
                <div><b class="underDescriptionElement">Converted Energy Cost: </b><b class="descriptionValue">{element.convertedEnergyCost} </b></div>
                <div><b class="underDescriptionElement">Cost: </b><b class="descriptionValue">{element.cost.map(element1=>{return <b> {element1}, </b>})}</b></div>
                <div><b class="underDescriptionElement">Damage: </b><b class="descriptionValue">{element.damage} </b></div>
                <div><b class="underDescriptionElement">Description: </b><b class="descriptionValue">{element.text} </b></div>
                <div><b class="underDescriptionElement">------------------------------------</b></div>
                </div>
            })}
        </div>
        <div>
          <b class="descriptionElement">Pokemon Resistances: </b>
          {
          pokemonCards.data && pokemonCards.data.resistances!==undefined && pokemonCards.data.resistances.map(element=>
            {
              return <b class="descriptionValue">{element.type}({element.value}), </b>
            })}
        </div>
        <div>
          <b class="descriptionElement">Pokemon Weaknesses: </b>
          {
          pokemonCards.data && pokemonCards.data.weaknesses!==undefined && pokemonCards.data.weaknesses.map(element=>
            {
              return <b class="descriptionValue">{element.type}({element.value}), </b>
            })}
        </div>
        <div>
          <b class="descriptionElement">Retreat Cost: </b>
          {
          pokemonCards.data && pokemonCards.data.retreatCost!==undefined && pokemonCards.data.retreatCost.map(element=>
            {
              return <b class="descriptionValue">{element}, </b>
            })}
        </div>
        <div>
          <b class="descriptionElement">------------------------------------Details------------------------------------</b>
        </div>
        <div>
          <b class="descriptionElement">Pokemon Rarity: </b>
          <b class="descriptionValue">{pokemonCards.data && pokemonCards.data.rarity!==undefined&&pokemonCards.data.rarity}</b>
        </div>
        <div>
          <b class="descriptionElement">Pokemon ID: </b>
          <b class="descriptionValue">{pokemonCards.data && pokemonCards.data.id},</b>
        </div>
        <div>
          <b class="descriptionElement">Artist: </b>
          <b class="descriptionValue">{pokemonCards.data&&pokemonCards.data.artist!==undefined && pokemonCards.data.artist}</b>
        </div>
        <div>
          <b class="descriptionElement">Supertype: </b>
          <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.supertype!==undefined&& pokemonCards.data.supertype}</b>
        </div>
        <div>
          <b class="descriptionElement">Evolves From: </b>
          <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.evolvesFrom!==undefined&& pokemonCards.data.evolvesFrom}</b>
        </div>
        <div>
          <b class="descriptionElement">Legalities: </b>
          <b class="underDescriptionElement">Unlimited: </b>
          <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.legalities.unlimited!==undefined&& pokemonCards.data.legalities.unlimited}, </b>
          <b class="underDescriptionElement">Standard: </b>
          <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.legalities.standard!==undefined&& pokemonCards.data.legalities.standard}, </b>
          <b class="underDescriptionElement">Expanded: </b>
          <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.legalities.expanded!==undefined&& pokemonCards.data.legalities.expanded}, </b>
        </div>
        <div>
          <b class="descriptionElement">Flavor Text: </b>
          <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.flavorText!==undefined&& pokemonCards.data.flavorText}</b>
        </div>
        <div>
          <b class="descriptionElement">------------------------------------Set------------------------------------ </b>
          <div>
            <div><b class="underDescriptionElement">Name: </b>
            <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.set.name!==undefined&& pokemonCards.data.set.name}</b></div>
            <div><b class="underDescriptionElement">Series: </b>
            <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.set.series!==undefined&& pokemonCards.data.set.series}</b></div>
            <div><b class="underDescriptionElement">Total Cards: </b>
            <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.set.total!==undefined&& pokemonCards.data.set.total}</b></div>
            <div><b class="underDescriptionElement">Cards Printed In Total: </b>
            <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.set.printedTotal!==undefined&& pokemonCards.data.set.printedTotal}</b></div>
            <div><b class="underDescriptionElement">Release Date: </b>
            <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.set.releaseDate!==undefined&& pokemonCards.data.set.releaseDate}</b></div>
            <div><b class="underDescriptionElement">Set ID: </b>
            <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.set.id!==undefined&& pokemonCards.data.set.id}</b></div>
            <div><b class="underDescriptionElement">Logo: </b></div>
            <div>
              <img class="descriptionImage"
              src={ pokemonCards.data &&pokemonCards.data.set.images.logo && pokemonCards.data.set.images.logo}
              alt={pokemonCards.data &&pokemonCards.data.set.images.logo &&  pokemonCards.data.set.images.logo+"_image_alt"}
              />
            </div>
            <div><b class="underDescriptionElement">Symbol: </b></div>
            <div>
              <img class="descriptionImage"
              src={pokemonCards.data &&pokemonCards.data.set.images.symbol &&  pokemonCards.data.set.images.symbol}
              alt={pokemonCards.data &&pokemonCards.data.set.images.symbol &&  pokemonCards.data.set.images.symbol+"_image_alt"}
              />
            </div>
            <div><b class="underDescriptionElement">ptcgoCode: </b>
            <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.set.ptcgoCode!==undefined&& pokemonCards.data.set.ptcgoCode}</b></div>
            <div><b class="underDescriptionElement">Updated At: </b>
            <b class="descriptionValue">{pokemonCards.data &&pokemonCards.data.set.updatedAt!==undefined&& pokemonCards.data.set.updatedAt}</b></div>
          </div>
          
        </div>
        

        
        
        
        
        
      </div>
    </div>
  
  )
}
export default Pokemon

/*
example objectfrom 'https://api.pokemontcg.io/v2/cards/'+match.params.pokemonID
.data{

artist: "Eske Yoshinob"
attacks: (2) [{…}, {…}]
convertedRetreatCost: 4
evolvesTo: ["M Venusaur-EX"]
hp: "180"
id: "xy1-1"
images: {small: "https://images.pokemontcg.io/xy1/1.png", large: "https://images.pokemontcg.io/xy1/1_hires.png"}
legalities: {unlimited: "Legal", expanded: "Legal"}
name: "Venusaur-EX"
nationalPokedexNumbers: [3]
number: "1"
rarity: "Rare Holo EX"
retreatCost: (4) ["Colorless", "Colorless", "Colorless", "Colorless"]
rules: ["Pokémon-EX rule: When a Pokémon-EX has been Knocked Out, your opponent takes 2 Prize cards."]
set: {id: "xy1", name: "XY", series: "XY", printedTotal: 146, total: 146, …}
subtypes: (2) ["Basic", "EX"]
supertype: "Pokémon"
tcgplayer: {url: "https://prices.pokemontcg.io/tcgplayer/xy1-1", updatedAt: "2021/05/24", prices: {…}}
types: ["Grass"]
weaknesses: [{…}]
}
*/